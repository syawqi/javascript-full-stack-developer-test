const Joi = require("@hapi/joi");
// Region validator
const RegionValidator = {
  region: {
    body: {
      name: Joi.required(),
      capital: Joi.required(),
      chief: Joi.required(),
      rise: Joi.number().required(),
      parent_id: Joi.number(),
    },
  },
};

module.exports = {
  RegionValidator: RegionValidator,
};
