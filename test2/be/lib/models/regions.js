'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Regions extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      this.hasMany(models.Regions,{foreignKey : 'parent_id', as : "reg"});
      this.belongsTo(models.Regions,{foreignKey : 'parent_id', as : "parent"});
    }
  };
  Regions.init({
    capital: DataTypes.STRING,
    chief: DataTypes.STRING,
    name: DataTypes.STRING,
    rise: DataTypes.INTEGER,
    parent_id: DataTypes.INTEGER,
    latitude: DataTypes.DOUBLE,
    longitude: DataTypes.DOUBLE
  }, {
    sequelize,
    modelName: 'Regions',
  });
  
  return Regions;
};