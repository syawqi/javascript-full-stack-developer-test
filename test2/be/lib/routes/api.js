var Router = require("@koa/router");
var jwt = require("koa-jwt");
const { ProvinceController, AuthController } = require("../controllers");
const joi = require("@hapi/joi");
const Validation = require("koa2-validation").Validation;
const validator = new Validation(joi);
const validate = validator.validate.bind(validator);

const { RegionValidator } = require("../validators/regions_validator");

// init Router
const router = new Router();

router.use(
  jwt({ secret: "rahasia" }).unless({
    path: [/\/api\/public*/, /\/api\/auth*/],
  })
);

// generate token
router.post(["/api/auth/issue"], AuthController.issue);

// regions

// get data
router.get(["/api/public/regions"], ProvinceController.index);

// create data
router.post(
  ["/api/regions"],
  validate(RegionValidator.region),
  ProvinceController.create
);

// update data
router.put(
  ["/api/regions/:id"],
  validate(RegionValidator.region),
  ProvinceController.update
);

// del data
router.del(["/api/regions/:regionId"], ProvinceController.delete);
// end regions
module.exports = router;
