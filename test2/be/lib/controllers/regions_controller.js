const { Regions } = require("../models");
const { Op } = require("sequelize");

// get all regions
var index = async (ctx, next) => {
  if (ctx.request.query.name) {
    // get regions by like name
    region = await Regions.findAll({
      include: ["reg","parent"],
      // passing query
      where: {
        name: { [Op.like]: "%" + ctx.request.query.name + "%" },
      },
    });
  } else {
    // get all region
    region = await Regions.findAll({
      include: ["reg","parent"],
    });
  }

  // return body
  return (ctx.body = {
    status: 200,
    data: region,
  });
};

// create region
var create = async (ctx, next) => {
  // create region
  await Regions.create({
    name: ctx.request.body["name"],
    capital: ctx.request.body["capital"],
    chief: ctx.request.body["chief"],
    rise: ctx.request.body["rise"],
    parent_id: ctx.request.body["parent_id"],
    latitude: ctx.request.body["latitude"],
    longitude: ctx.request.body["longitude"],
  });
  // return body
  ctx.body = {
    status: 200,
    message: "Data has been created",
  };
};

// update region
var update = async (ctx, next) => {
  // update region
  await Regions.update(
    {
      name: ctx.request.body["name"],
      capital: ctx.request.body["capital"],
      chief: ctx.request.body["chief"],
      rise: ctx.request.body["rise"],
      parent_id: ctx.request.body["parent_id"],
    },
    {
      // passing parameter
      where: {
        id: ctx.params.id,
      },
    }
  );

  // return body
  ctx.body = {
    status: 200,
    message: "Data has been update",
  };
};

// delete region
var deleteRegion = async (ctx, next) => {
  // delete region
  await Regions.destroy({
    where: {
      id: ctx.params.regionId,
    },
  });

  // return body
  return (ctx.body = {
    status: 200,
    message: "Data has been delete",
  });
};

module.exports = {
  create: create,
  index: index,
  delete: deleteRegion,
  update: update,
};
