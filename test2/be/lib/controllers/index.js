var ProvinceController = require("./regions_controller");
var AuthController = require("./auth_controller");

// export defined controller
module.exports = {
  AuthController,
  ProvinceController,
};
