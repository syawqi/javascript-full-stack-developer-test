const jsonwebtoken = require('jsonwebtoken');

// issue token
var issue = async (ctx, next) => {
  //   generate token
  ctx.body = {
    status: 200,
    // rahasia is secret key
    token: jsonwebtoken.sign({}, 'rahasia'),
  };
};

module.exports = {
  issue: issue,
};
