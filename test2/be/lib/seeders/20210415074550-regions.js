"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */

    await queryInterface.bulkInsert(
      "Regions",
      [
        {
          id : 1,
          capital: "Kota Samarinda",
          chief: "Isran Noor",
          name: "Kalimantan Timur",
          rise: 1997,
          parent_id : null,
          latitude :  -0.502106,
          longitude : 117.153709,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          id : 2,
          capital: "Samarinda",
          chief: "Andi Harun",
          name: "Kota Samarinda",
          rise: 1997,
          parent_id : 1,
          latitude :  -0.502106,
          longitude : 117.153709,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          id : 3,
          capital: "Balikpapan",
          chief: "Rahmad Ma'sud",
          name: "Kota Balikpapan",
          latitude : -1.265386,
          longitude : 116.831200,
          rise: 1997,
          parent_id : 1,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ],
      {}
    );
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    return queryInterface.bulkDelete("Regions", null, {});
  },
};
