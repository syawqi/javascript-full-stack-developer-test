var Koa = require("koa");
var apiRoute = require("./lib/routes/api.js");
const bodyParser = require("koa-bodyparser");
const ngrok = require('ngrok');

var app = new Koa();
var cors = require('koa2-cors');
// catch validator and error
app.use(async (ctx, next) => {
  try {
    await next();
  } catch (err) {
    ctx.status = err.status || err.code;
    ctx.body = {
      success: false,
      message: err.message,
    };
  }
});

app.use(cors());

app.use(bodyParser());

app.use(apiRoute.routes()).use(apiRoute.allowedMethods());

app.listen(3001);

ngrok.connect({
  proto : 'http',
  addr : process.env.PORT,
}, (err, url) => {
  if (err) {
      console.error('Error while connecting Ngrok',err);
      return new Error('Ngrok Failed');
  }
});