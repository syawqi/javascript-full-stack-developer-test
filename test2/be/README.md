# BE Test
## Getting Started <a name = "getting_started"></a>

Untuk melakukan ujicoba aplikasi. Ketika pertama kali di clone dan belum terdapat node_modules didalam project, maka lakukan 
```
yarn install
```
atau
```
npm install
```

Kemudian untuk menjalankan dapat menggunakan perintah.

```
yarn start
```
atau
```
npm run start
```

Aplikasi ini menggunakan sequelize, untuk dokumentasi dapat dibaca [disini](https://sequelize.org/master/manual/getting-started.html)

Sebelum menjalankan server atau sequelize pastikan anda sudah melakukan config pada folder lib/config/config.json, sesuaikan dengan configurasi servernya.

Saat ini server hanya terintegrasi dengan mariadb dan mysql.
