import config from "./config";

const getRegions = async (name) => {
  let data = await fetch(config.baseUrl + "public/regions?=" + name);

  return data.json();
};

const deleteRegions = async (id, token) => {
  await fetch(config.baseUrl + "regions/" + id, {
    method: "DELETE",
    headers: {
      Authorization: "Bearer " + token,
    },
  });

  return;
};

const getToken = async () => {
  let data = await fetch(config.baseUrl + "auth/issue", {
    method: "POST",
  });

  return data.json();
};

const postRegion = async (body, token) => {
  console.log();
  try {
    let req = await fetch(config.baseUrl + "regions", {
      method: "POST",
      body: body,
      headers: {
        Authorization: "Bearer " + token,
        "Content-Type": "application/json",
      },
    });

    console.log(req.json());

    if (req.status == 200) return "";
    else throw "error";
  } catch (error) {
    console.log(error);
    throw error;
  }
};

const request = {
  getRegions,
  getToken,
  deleteRegions,
  postRegion,
};
export default request;
