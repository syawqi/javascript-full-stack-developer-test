import { Redirect, Route } from "react-router-dom";
import AccessToken from "../utils/local";

function PrivateRoute({ component: Component, ...rest }) {
  const [token, setToken] = AccessToken();

  if (token)
    return <Route {...rest} render={(props) => <Component {...props} />} />;

  return <Redirect to="/" />;
}

export default PrivateRoute;
