import Region from "./region";
import { BrowserRouter as Router, Switch } from "react-router-dom";
import Login from "./auth/login";
import RegionList from "./region/list";
import AccessToken from "../utils/local";
import PublicRoute from "../routes/public";
import PrivateRoute from "../routes/private";
import FormRegion from "./region/form";

function Index() {
  const [token, setToken] = AccessToken();
  return (
    <Router>
      <div>
        <Switch>
          <PublicRoute exact path="/" component={Region}></PublicRoute>
          <PublicRoute path="/login" component={Login}></PublicRoute>
          <PrivateRoute path="/admin" component={RegionList}></PrivateRoute>
          <PrivateRoute path="/form" component={FormRegion}></PrivateRoute>
        </Switch>
      </div>
    </Router>
  );
}

export default Index;
