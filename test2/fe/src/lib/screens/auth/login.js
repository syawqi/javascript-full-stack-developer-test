//this screen is for get authorization token(JWT)

import Navbar from "../../components/nav_bar";
import AccessToken from "../../utils/local";
import request from "../../networks/request";
import { useHistory } from "react-router-dom";

function Login() {
  const [token, setToken] = AccessToken();
  const history = useHistory();

  async function generateToken() {
    const token = await request.getToken();

    setToken(token["token"]);
  }
  return (
    <div>
      {/* init navbar */}
      <Navbar></Navbar>
      <form className="w-full flex flex-wrap">
        <div className="w-1/3 ml-auto h-12"></div>
        <div className="flex items-center border-b border-teal-500 py-2 mt-10 w-1/3">
          <div className="appearance-none bg-transparent border-none w-full text-gray-700 mr-3 py-1 px-2 leading-tight focus:outline-none flex-grow break-all">
            {token}
          </div>
          <button
            className="flex-shrink-0 bg-green-500 text-white border-transparent border-4 text-teal-500 hover:text-teal-800 text-sm py-1 px-2 rounded"
            type="button"
            onClick={async () => {
              await generateToken();
              history.replace("/");
            }}
          >
            Generate Token
          </button>
        </div>
        <div className="w-1/3 ml-auto h-12"></div>
      </form>
    </div>
  );
}

export default Login;
