import NavBar from "../../components/nav_bar";
import { useState, useEffect } from "react";
import req from "../../networks/request";
import { GoogleApiWrapper, Map, InfoWindow, Marker } from "google-maps-react";
import config from "../../networks/config";

function Region({ google }) {
  const [regions, setRegions] = useState([]);
  const [selectedPlace, setSelectedPlace] = useState({
    name: [],
  });
  const [activeMarker, setActiveMarker] = useState({});
  const [showingInfoWindow, setShowingInfoWindow] = useState(false);

  useEffect(() => {
    async function getRegions() {
      let data = await req.getRegions();

      setRegions(data.data);

      return;
    }

    getRegions();
  }, []);

  var onMarkerClick = (props, marker, e) => {
    setSelectedPlace(JSON.parse(props.name));
    setActiveMarker(marker);
    setShowingInfoWindow(true);
  };

  var onMapClicked = (props) => {
    if (showingInfoWindow) {
      setShowingInfoWindow(false);
      setActiveMarker(null);
    }
  };

  return (
    <div className="h-full">
      <NavBar></NavBar>
      <div style={{ height: "100vh", width: "100%" }}>
        <Map
          google={google}
          zoom={8}
          onClick={onMapClicked}
          initialCenter={{
            lat: -0.502106,
            lng: 117.153709,
          }}
        >
          {regions.map((res, index) => {
            console.log(res);
            return (
              <Marker
                key={index}
                name={JSON.stringify(res)}
                onClick={onMarkerClick}
                position={{
                  lat: res.latitude,
                  lng: res.longitude,
                }}
              />
            );
          })}
          <InfoWindow marker={activeMarker} visible={showingInfoWindow}>
            {selectedPlace ? (
              <div>
                <h1>Nama : {selectedPlace.name}</h1>
                <h1>Pemimpin : {selectedPlace.chief}</h1>
                <h1>Ibu Kota : {selectedPlace.capital}</h1>
                <h1>Tahun Berdiri : {selectedPlace.rise}</h1>
                {selectedPlace.parent_id ? (
                  <h1>Bagian Dari : {selectedPlace.parent?.name ?? "-"}</h1>
                ) : null}
              </div>
            ) : null}
          </InfoWindow>
        </Map>
      </div>
    </div>
  );
}

export default GoogleApiWrapper({
  apiKey: config.googleToken,
})(Region);
