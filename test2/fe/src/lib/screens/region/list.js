import NavBar from "../../components/nav_bar";
import AccessToken from "../../utils/local";
import request from "../../networks/request";
import { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import swal from 'sweetalert';

function RegionList() {
  const [token, setToken] = AccessToken();
  const [regions, setRegions] = useState([]);

  async function getRegions() {
    let _reg = await request.getRegions();

    setRegions(_reg["data"]);
  }

  useEffect(() => {
    getRegions();
  }, []);

  async function deleteRegion(id) {
    await request.deleteRegions(id, token);

    getRegions();

    swal("Success", "Data telah terhapus", "success");
  }

  return (
    <div>
      <NavBar></NavBar>
      <div className="container mx-auto px-4 sm:px-8">
        <div className="py-8">
          <div>
            <h2 className="text-2xl font-semibold leading-tight mb-2">Regional</h2>
            <Link to="/form" className="bg-blue-500 hover:bg-blue-300 text-white font-bold py-2 mr-2 px-4 rounded text-xs">
              Tambah
            </Link>
          </div>
          <div className="-mx-4 sm:-mx-8 px-4 sm:px-8 py-4 overflow-x-auto">
            <div className="inline-block min-w-full shadow rounded-lg overflow-hidden">
              <table className="min-w-full leading-normal">
                <thead>
                  <tr>
                    <th className="px-5 py-3 border-b-2 border-gray-200 bg-gray-100 text-center text-xs font-semibold text-gray-600 uppercase tracking-wider">
                      Nama
                    </th>
                    <th className="px-5 py-3 border-b-2 border-gray-200 bg-gray-100 text-center text-xs font-semibold text-gray-600 uppercase tracking-wider">
                      Pemimpin.
                    </th>
                    <th className="px-5 py-3 border-b-2 border-gray-200 bg-gray-100 text-center text-xs font-semibold text-gray-600 uppercase tracking-wider">
                      Beridiri.
                    </th>
                    <th className="px-5 py-3 border-b-2 border-gray-200 bg-gray-100 text-center text-xs font-semibold text-gray-600 uppercase tracking-wider">
                      Bagian Dari
                    </th>
                    <th className="px-5 py-3 border-b-2 border-gray-200 bg-gray-100 text-center text-xs font-semibold text-gray-600 uppercase tracking-wider">
                      Wilayah Turunan
                    </th>
                    <th className="px-5 py-3 border-b-2 border-gray-200 bg-gray-100 text-center text-xs font-semibold text-gray-600 uppercase tracking-wider">
                      Aksi
                    </th>
                  </tr>
                </thead>
                <tbody>
                  {regions.map((res, index) => (
                    <tr key={index}>
                      <td className="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                        <p className="text-gray-900 whitespace-no-wrap ">
                          {res["name"]}
                        </p>
                      </td>
                      <td className="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                        <p className="text-gray-900 whitespace-no-wrap ">
                          {res["chief"]}
                        </p>
                      </td>
                      <td className="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                        <p className="text-gray-900 whitespace-no-wrap text-center">
                          {res["rise"]}
                        </p>
                      </td>
                      <td className="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                        <p className="text-gray-900 whitespace-no-wrap ">
                          {res?.parent?.name ?? ""}
                        </p>
                      </td>
                      <td className="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                        <p className="text-gray-900 whitespace-no-wrap text-center">
                          {res?.reg?.length ?? 0}
                        </p>
                      </td>
                      <td className="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                        <button
                          className="bg-red-500 hover:bg-red-300 text-white font-bold py-2 px-4 rounded"
                          onClick={() => deleteRegion(res["id"])}
                        >
                          Hapus
                        </button>
                      </td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default RegionList;
