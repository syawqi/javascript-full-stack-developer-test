import NavBar from "../../components/nav_bar";
import AccessToken from "../../utils/local";
import request from "../../networks/request";
import { useState, useEffect } from "react";
import swal from "sweetalert";
import { useHistory } from "react-router-dom";

function FormRegion() {
  const history = useHistory();

  const [token, setToken] = AccessToken();
  const [regions, setRegions] = useState([]);
  const [name, setname] = useState(null);
  const [capital, setcapital] = useState(null);
  const [chief, setchief] = useState(null);
  const [rise, setrise] = useState(null);
  const [parent, setparent] = useState(null);
  const [latitude, setlatitude] = useState(0);
  const [longitude, setlongitude] = useState(0);

  async function getRegions() {
    let _reg = await request.getRegions();

    setRegions(_reg["data"]);
  }

  async function create() {
    const body = {
      name: name,
      capital: capital,
      chief: chief,
      rise: rise,
      latitude: latitude,
      longitude: longitude,
    };

    if (parent) {
      body.parent_id = parent;
    }

    console.log(body);

    try {
      await request.postRegion(JSON.stringify(body), token);
      swal("Success", "Data telah ditambahkan", "success");
      history.push("/admin");
    } catch (error) {
      swal("Failed", "Data gagal ditambahkan", "error");
    }
  }

  useEffect(() => {
    getRegions();
  }, []);

  return (
    <div>
      {/* init navbar */}
      <NavBar></NavBar>
      <div className="container mx-auto px-4 sm:px-8">
        <div className="py-8">
          <div>
            <h2 className="text-2xl font-semibold leading-tight mb-2">
              Form Regional
            </h2>
          </div>
          {/* init Form */}
          <div className="w-full">
            <div className="flex flex-wrap -mx-3 mb-6">
              <div className="w-full md:w-1/2 px-3 mb-6 md:mb-0">
                <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                  Nama
                </label>
                <input
                  className="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white"
                  id="grid-first-name"
                  type="text"
                  onChange={(v) => setname(v.target.value)}
                  required
                />
              </div>
              <div className="w-full md:w-1/2 px-3">
                <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                  Ibu Kota
                </label>
                <input
                  className="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                  id="grid-last-name"
                  required
                  type="text"
                  onChange={(v) => setcapital(v.target.value)}
                />
              </div>
            </div>
            <div className="flex flex-wrap -mx-3 mb-6">
              <div className="w-full px-3">
                <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                  Pemimpin
                </label>
                <input
                  className="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                  id="grid-password"
                  onChange={(v) => setchief(v.target.value)}
                  required
                />
              </div>
            </div>
            <div className="flex flex-wrap -mx-3 mb-6">
              <div className="w-full md:w-1/2 px-3 mb-6 md:mb-0">
                <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                  Tahun
                </label>
                <input
                  className="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                  id="grid-city"
                  type="number"
                  onChange={(v) => setrise(v.target.value)}
                  required
                />
              </div>
              <div className="w-full md:w-1/2 px-3 mb-6 md:mb-0">
                <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                  Kota Bagian
                </label>
                <div className="relative">
                  <select
                    className="block appearance-none w-full bg-gray-200 border border-gray-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                    id="grid-state"
                    onChange={(v) => setparent(v.target.value)}
                  >
                    <option value=""></option>
                    {regions.map((res, index) => (
                      <option key={index} value={res.id}>
                        {res.name}
                      </option>
                    ))}
                  </select>
                  <div className="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
                    <svg
                      className="fill-current h-4 w-4"
                      xmlns="http://www.w3.org/2000/svg"
                      viewBox="0 0 20 20"
                    >
                      <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z" />
                    </svg>
                  </div>
                </div>
              </div>
            </div>
            <div className="flex flex-wrap -mx-3 mb-2">
              <div className="w-full md:w-1/2 px-3 mb-6 md:mb-0">
                <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                  Latitude
                </label>
                <input
                  className="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                  id="grid-city"
                  onChange={(v) => setlatitude(v.target.value)}
                />
              </div>
              <div className="w-full md:w-1/2 px-3 mb-6 md:mb-0">
                <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                  Longitude
                </label>
                <input
                  className="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                  id="grid-city"
                  onChange={(v) => setlongitude(v.target.value)}
                />
              </div>
            </div>
            <button
              className="bg-blue-500 hover:bg-blue-300 text-white font-bold py-2 mt-6 px-4 rounded"
              onClick={() => create()}
            >
              Buat Data
            </button>
          </div>
        </div>
      </div>
    </div>
  );
}

export default FormRegion;
