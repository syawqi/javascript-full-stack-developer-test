import { useState, useEffect} from 'react';

const AccessToken = () => {
  const [token, setToken] = useState(
    localStorage.getItem('token') || ""
  );

  useEffect(() => {
    localStorage.setItem('token', token);
  }, [token]);

  return [token, setToken];
};

export default AccessToken;
