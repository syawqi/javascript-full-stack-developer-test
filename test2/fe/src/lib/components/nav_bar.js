import { Link, useHistory } from "react-router-dom";
import AccessToken from "../utils/local";

export default function NavBar() {
  const [token, setToken] = AccessToken();
  const history = useHistory();

  return (
    <nav className="flex items-center justify-between bg-green-600 flex-wrap bg-teal-500 p-6">
      {/* LOGO/ Icon */}
      <div className="flex items-center flex-shrink-0 text-white mr-6">
        <Link to="/">
          <span className="font-semibold text-xl tracking-tight">
            Data Induk Regional
          </span>
        </Link>
      </div>

      {/* Nav Bar */}
      <div className="w-full block flex-grow text-white lg:flex lg:items-center lg:w-auto">
        <div className="text-sm lg:flex-grow">
          {/* Region */}
          <Link
            to="/"
            className="block mt-4 lg:inline-block lg:mt-0 text-teal-200 hover:text-white mr-4 hover:text-green-400"
          >
            Region
          </Link>

          {/* Validation Login or Not */}
          {!token ? (
            <Link
              to="/login"
              className="block mt-4 lg:inline-block lg:mt-0 text-teal-200 hover:text-white mr-4 hover:text-green-400"
            >
              Masuk
            </Link>
          ) : (
            <div className="block mt-4 lg:inline-block lg:mt-0 ">
              <Link
                to="/admin"
                className="block mt-4 lg:inline-block lg:mt-0 text-teal-200 hover:text-white mr-4 hover:text-green-400"
              >
                Administrator
              </Link>
              <Link
                to=""
                onClick={async () => {
                  await setToken("");
                  history.replace("/");
                }}
                className="block mt-4 lg:inline-block lg:mt-0 text-teal-200 hover:text-white mr-4 hover:text-green-400"
              >
                Keluar
              </Link>
            </div>
          )}
        </div>
      </div>
    </nav>
  );
}
