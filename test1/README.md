# Test 1
## Getting Started <a name = "getting_started"></a>

Untuk melakukan ujicoba aplikasi. 
Silahkan ketikan perintah : 
```
node <problem>.js
```

contoh 

```
node problem1.js
```

kemudian masukan array yang diinginkan dengan cara memisahkan angka dengan koma(,)

contoh 

```
1,2,3,4,5
```