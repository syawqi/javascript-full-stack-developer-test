const readline = require("readline");

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

function sort(arr) {
  console.log(arr.sort());
}

rl.question("Insert array object and split with comma(,) ", (answer) => {
  var arr = answer.split(",");
  sort(arr);

  rl.close();
});
