const readline = require("readline");

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

function toMax(arr, n) {
  let result = arr[0];
  let result2 = arr[0];
  let arrRes = [];

  for (let i = 0; i < n; i++) {
    let mul = arr[i];
    for (let j = i + 1; j < n; j++) {
      result = Math.max(result, mul);
      mul *= arr[j];
    }
    result = Math.max(result, mul);
  }

  for (let i = 0; i < n; i++) {
    let mul = arr[i];
    for (let j = i + 1; j < n; j++) {
      result2 = Math.max(result2, mul);
      mul *= arr[j];
    }
    result2 = Math.max(result2, mul);

    if (result2 == result) {
      arrRes.push(arr[i]);
    }
  }

  console.log(
    "the maximum product sub-array is " + arrRes + " having product " + result
  );
}

rl.question("Insert array object and split with comma(,) ", (answer) => {
  var arr = answer.split(",");
  toMax(arr, arr.length);

  rl.close();
});
