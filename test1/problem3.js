const readline = require("readline");

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

function CombinationRepetition(arr, size, com) {
  let currentArray = [];
  let tempArray = [];
  let currentI;
  for (let i = 0; i < arr.length; i++) {
    currentI = arr[i];
    for (let j = 0; j < arr.length; j++) {
      let _com = [currentI, arr[j]];

      tempArray.push(_com);
    }
    currentArray.push(tempArray);
    console.log(currentArray);
  }
}

rl.question("Insert array object and split with comma(,) ", (answer) => {
  var arr = answer.split(",");
  CombinationRepetition(arr, arr.length, 2);

  rl.close();
});
