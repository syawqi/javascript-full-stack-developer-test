const readline = require("readline");

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

function repeat(arr, size) {
  var i, j;
  let arrRes = [];
  for (i = 0; i < size; i++) {
    for (j = i + 1; j < size; j++) {
      if (arr[i] == arr[j]) arrRes.push(arr[i]);
    }
  }
  console.log("the duplicate element is " + arrRes);
}

rl.question("Insert array object and split with comma(,) ", (answer) => {
  var arr = answer.split(",");
  repeat(arr, arr.length);

  rl.close();
});
